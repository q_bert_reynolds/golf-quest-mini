﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "GolfQuestMini/Circle Cutout" {
  Properties {
    [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
    _Radius ("Radius", Float) = 0
    _Params ("Center (xy)", Vector) = (0.5, 0.5, 0, 0)
    [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
  }

  SubShader {
    Tags { 
      "Queue"="Transparent" 
      "IgnoreProjector"="True" 
      "RenderType"="Transparent" 
      "PreviewType"="Plane"
      "CanUseSpriteAtlas"="True"
    }

    Cull Off
    Lighting Off
    ZWrite Off
    Fog { Mode Off }
    Blend One OneMinusSrcAlpha

    Pass {
      CGPROGRAM
      #pragma vertex vert
      #pragma fragment frag
      #pragma multi_compile DUMMY PIXELSNAP_ON
      #include "UnityCG.cginc"
      
      struct appdata_t {
        float4 vertex   : POSITION;
        float4 color    : COLOR;
        float2 texcoord : TEXCOORD0;
      };

      struct v2f {
        float4 vertex   : SV_POSITION;
        fixed4 color    : COLOR;
        half2 texcoord  : TEXCOORD0;
        half2 screenPos : TEXCOORD1;
      };
      
      
      v2f vert(appdata_t IN) {
        v2f OUT;

        float4 v = UnityObjectToClipPos(IN.vertex);
		OUT.vertex = v;

		v.xy /= v.w;
		v.xy = 0.5*(v.xy+1.0);
		v.xy *= _ScreenParams.xy;
		OUT.screenPos = v.xy;

        OUT.texcoord = IN.texcoord;
        OUT.color = IN.color;
        #ifdef PIXELSNAP_ON
        OUT.vertex = UnityPixelSnap (OUT.vertex);
        #endif
        return OUT;
      }

      sampler2D _MainTex;
      float4 _Params;
      float _Radius;

      fixed4 frag(v2f IN) : SV_Target {
        fixed4 c = tex2D(_MainTex, IN.texcoord) * IN.color;

        half2 p = IN.screenPos - _Params.xy;
        float r = sqrt(p.x * p.x + p.y * p.y);
        
        if (r < _Radius) {
          c.a = 0;
        }

        return c;
      }
      ENDCG
    }
  }
}
