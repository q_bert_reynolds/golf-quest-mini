// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "GolfQuestMini/Radial Putt Meter" {
  Properties {
    [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
    _Green ("Green Color", Color) = (0,1,0)
    _GreenRange ("Green Range", Range(0,1)) = 0.9
    _Yellow ("Yellow Color", Color) = (1,1,0)
    _YellowRange ("Yellow Range", Range(0,1)) = 0.2
    _Red ("Red Color", Color) = (1,0,0)
    _RedRange ("Red Range", Range(0,1)) = 0.1
    _Angle ("Angle", Range(0,360)) = 0
    _InnerR ("Inner Radius", Range(0,1)) = 0.1
    _OuterR ("Outer Radius", Range(0,1)) = 0.1
    _YScale ("Y Scale", Range(0,1)) = 1
    _Params ("Center (xy), Rotation Pt (zw)", Vector) = (0.5, 0.5, 0.5, 0.5)
    [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
  }

  SubShader {
    Tags { 
      "Queue"="Transparent" 
      "IgnoreProjector"="True" 
      "RenderType"="Transparent" 
      "PreviewType"="Plane"
      "CanUseSpriteAtlas"="True"
    }

    Cull Off
    Lighting Off
    ZWrite Off
    Fog { Mode Off }
    Blend One OneMinusSrcAlpha

    Pass {
      CGPROGRAM
      #pragma vertex vert
      #pragma fragment frag
      #pragma multi_compile DUMMY PIXELSNAP_ON
      #include "UnityCG.cginc"
      
      struct appdata_t {
        float4 vertex   : POSITION;
        float4 color    : COLOR;
        float2 texcoord : TEXCOORD0;
      };

      struct v2f {
        float4 vertex   : SV_POSITION;
        fixed4 color    : COLOR;
        half2 texcoord  : TEXCOORD0;
        half2 worldPos  : TEXCOORD1;
      };
      
      
      v2f vert(appdata_t IN) {
        v2f OUT;
        OUT.vertex = UnityObjectToClipPos(IN.vertex);
        OUT.texcoord = IN.texcoord;
        OUT.color = IN.color;
        #ifdef PIXELSNAP_ON
        OUT.vertex = UnityPixelSnap (OUT.vertex);
        #endif

        OUT.worldPos = IN.vertex.xy;

        return OUT;
      }

      float4 _Green;
      float4 _Yellow;
      float4 _Red;
      sampler2D _MainTex;
      float4 _Params;
      float _Angle;
      float _YScale;
      float _InnerR;
      float _OuterR;

      fixed4 frag(v2f IN) : SV_Target {
        fixed4 c = tex2D(_MainTex, IN.texcoord);

        half2 uv = IN.worldPos - _Params.xy;
        float r = sqrt(uv.x * uv.x + uv.y * uv.y);
        uv = IN.worldPos - _Params.zw;
        float a = fmod(degrees(atan2(uv.y, uv.x)) + 180, 360);

        if (r > _InnerR) {
          if (a < _Angle) {
            c.rgb *= IN.color.rgb;
            c.rgb += IN.color.rgb;
          }

          //if (a > _MaxAngle / 360) {
          //  c.a = 0;
          //}
        }

        c.rgb *= c.a;
        return c;
      }
      ENDCG
    }
  }
}
