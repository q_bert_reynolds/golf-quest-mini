﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class CharacterInfoUI : MonoBehaviour, ISelectHandler, ISubmitHandler, IPointerClickHandler {

	public delegate void OnCharacterSelected (CharacterSheet characterSheet);
	public static event OnCharacterSelected onCharacterSelected = delegate {};

	public delegate void OnCharacterHighlighted (CharacterSheet characterSheet);
	public static event OnCharacterHighlighted onCharacterHighlighted = delegate {};

	public CharacterSheet characterSheet;
	public Text nameText;
	public Text healthText;
	public Text levelText;
	public Slider healthBar;

	void Start () {
		UpdateInfo();
	}
	
	[ContextMenu("Update Info")]
	public void UpdateInfo () {
		nameText.text = characterSheet.characterName;
		healthText.text = characterSheet.currentHealth + "/" + characterSheet.health;
		levelText.text = "Lv " + characterSheet.level;
		healthBar.value = Mathf.Clamp01((float)characterSheet.currentHealth / (float)characterSheet.health);
	}

	public void OnSelect (BaseEventData eventData) {
		onCharacterHighlighted(characterSheet);
	}

	public void OnSubmit (BaseEventData eventData) {
		onCharacterSelected(characterSheet);
	}

	public void OnPointerClick (PointerEventData eventData) {
		onCharacterSelected(characterSheet);
	}
}
