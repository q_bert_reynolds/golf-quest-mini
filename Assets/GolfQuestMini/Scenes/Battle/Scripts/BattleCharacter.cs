﻿using UnityEngine;
using System.Collections;
using Paraphernalia.Utils;
using Paraphernalia.Extensions;

public class BattleCharacter : MonoBehaviour {

	public delegate void OnBallFlightFinished (GolfClub club, float modifier, Vector3 target);
	public static event OnBallFlightFinished onBallFlightFinished = delegate {};

	public delegate void OnTurnFinished ();
	public static event OnTurnFinished onTurnFinished = delegate {};

	public CharacterSheet stats;
	public GameObject ball;
	public float poseDuration = 1.5f;

	private Vector3 ballPosition;
	private Animator animator;
	private GolfClub currentClub;
	private float modifier;
	private Enemy enemy;

	void Awake () {
		animator = GetComponent<Animator>();
		ballPosition = ball.transform.localPosition;
		Idle();
	}

	public void Idle () {
		animator.Play("Idle");
	}

	public void Ready () {
		animator.Play("Ready");
	}

	public void Swing (GolfClub club, float charge, Enemy enemy) {
		this.enemy = enemy;
		currentClub = club;
		modifier = charge;
		animator.Play("Swing");
		ball.SetActive(true);
	}

	public void HitBall () {
		StartCoroutine("BallFlightCoroutine");
	}

	IEnumerator BallFlightCoroutine () {
		Vector3 startPos = ball.transform.position;
		Vector3 endPos = enemy.gameObject.RendererBounds().center;
		float height = currentClub.loft / 10f;
		float duration = currentClub.loft / 50f;

		float t = 0;
		while (t < duration) {
			t += Time.deltaTime;
			float frac = Mathf.Clamp01(t / duration);
			Vector3 x = Vector3.Lerp(startPos, endPos, frac);
			float h = 0;
			if (frac < 0.5f) h = Interpolate.EaseOutQuad(frac * 2);
			else h = 1-Interpolate.EaseInQuad((frac-0.5f) * 2);
			Vector3 y = Vector3.up * height * h;
			ball.transform.position = x + y;
			yield return new WaitForEndOfFrame();
		}

		onBallFlightFinished(currentClub, modifier, endPos);

		enemy.TakeDamage(stats.CalculateDamage(enemy.stats, modifier));
		ball.SetActive(false);
		ball.transform.localPosition = ballPosition;

		yield return new WaitForSeconds(poseDuration);
		onTurnFinished();
		Idle();
	}

	public void Putt (GolfClub club, float accuracy, Enemy enemy) {
		currentClub = club;
		modifier = accuracy;
		animator.Play("Putt");
	}

	public void TakeHit () {

	}
}
