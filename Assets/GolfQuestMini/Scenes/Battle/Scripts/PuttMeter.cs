﻿using UnityEngine;
using System.Collections;
using Paraphernalia.Utils;

[ExecuteInEditMode]
public class PuttMeter : MonoBehaviour {

	public delegate void OnChargeCancelled ();
	public static event OnChargeCancelled onChargeCancelled = delegate {};

	public delegate void OnChargeStarted ();
	public static event OnChargeStarted onChargeStarted = delegate {};

	public delegate void OnChargeFinished (float charge);
	public static event OnChargeFinished onChargeFinished = delegate {};

	[Range(0,1)] public float pct = 0;
	public float chargeTime = 1;
	public Interpolate.EaseType easeInType;
	public Interpolate.EaseType easeOutType;

	public float maxAngle = 279f;
	public float[] stages = new float[] {1f, 0.94f, 0.86f};
	
	private int _stage = 0;
	public int stage {
		get { return Mathf.Clamp(_stage, 0, stages.Length - 1); }
	}

	public int nextStage {
		get { return Mathf.Clamp(_stage + 1, 0, stages.Length - 1); }
	}

	public Material mat {
		get {
			if (Application.isPlaying) return GetComponent<Renderer>().material;
			return GetComponent<Renderer>().sharedMaterial;
		}
	}

	public float waitTime = 0.2f;
	private float lastReset = 0;
	private enum SwingState {
		Waiting,
		Ready,
		Started, 
		Finished
	}
	private SwingState swingState = SwingState.Waiting;

	void Start () {
		ResetCharge();
	}

	public void Cancel () {
		onChargeCancelled();
	}

	[ContextMenu("Start Charge")]
	public void StartCharge () {
		StartCoroutine("ChargeCoroutine");
		onChargeStarted();
	}

	[ContextMenu("Reset Charge")]
	public void ResetCharge () {
		lastReset = Time.time;
		swingState = SwingState.Waiting;
		mat.SetFloat("_MaxAngle", maxAngle);
		_stage = 0;
		pct = 0;
	}

	IEnumerator ChargeCoroutine () {
		while (enabled) {
			float t = 0;
			float frac = 0;
			while (t < chargeTime) {
				t += Time.deltaTime;
				frac = Mathf.Clamp01(t / chargeTime);
				pct = stages[stage] * Interpolate.Ease(easeInType, frac);
				yield return new WaitForEndOfFrame();
			}
			while (frac > stages[nextStage]) {
				t -= Time.deltaTime;
				frac = Mathf.Clamp01(t / chargeTime);
				pct = stages[stage] * Interpolate.Ease(easeInType, frac);
				yield return new WaitForEndOfFrame();
			}
			mat.SetFloat("_MaxAngle", stages[nextStage] * maxAngle);
			while (t > 0) {
				t -= Time.deltaTime;
				frac = Mathf.Clamp01(t / chargeTime);
				pct = stages[stage] * Interpolate.Ease(easeInType, frac);
				yield return new WaitForEndOfFrame();
			}
			_stage = nextStage;
		}
	}

	[ContextMenu("Set Charge")]
	void SetCharge () {
		StopCoroutine("ChargeCoroutine");
		onChargeFinished(pct);
	}

	void Update () {
		mat.SetFloat("_Angle", pct * maxAngle);

		#if UNITY_EDITOR
		if (Application.isPlaying) {
		#endif

			bool actionButtonDown = Input.GetButtonDown("Submit");
			bool cancelButtonDown = Input.GetButtonDown("Cancel");

			switch (swingState) {
				case SwingState.Waiting:
					if (cancelButtonDown) {
						onChargeCancelled();
					}
					if (Time.time - lastReset > waitTime) {
						swingState = SwingState.Ready;
					}
					break;					
				case SwingState.Ready:
					if (cancelButtonDown) {
						onChargeCancelled();
					}
					else if (actionButtonDown) {
						swingState = SwingState.Started;
						StartCharge();
					}
					break;					
				case SwingState.Started:
					if (actionButtonDown) {
						swingState = SwingState.Finished;
						SetCharge();
					}
					break;					
				case SwingState.Finished:
					break;
			}

		#if UNITY_EDITOR
		}
		#endif
	}
}
