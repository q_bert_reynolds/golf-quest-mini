﻿using UnityEngine;
using System.Collections;
using Paraphernalia.Utils;
using Paraphernalia.Extensions;

public enum BattleAction {
	None,
	Stroke,
	Tech,
	Item,
	Mulligan
}

public class BattleSystem : MonoBehaviour {

	public delegate void OnBattleStateChanged();
	public static event OnBattleStateChanged onBattleFinish = delegate {};
	
	public Camera cam;
	public BattleMenu battleMenu;
	public SwingMeter swingMeter;
	public GameObject enemyHealthBar;
	public Transform enemiesRoot;
	public Transform playersRoot;
	public Explosion explosion;
	public Enemy enemy; // public enemy, tee hee

	[Space(10)]
	public float transitionInTime = 1.5f;
	public Interpolate.EaseType transitionInEase = Interpolate.EaseType.InOutQuad;

	[Space(10)]
	public float transitionOutTime = 0.5f;
	public Interpolate.EaseType transitionOutEase = Interpolate.EaseType.InOutQuad;
	
	[Space(10)]
	public float moveToSwingPositionTime = 1;
	public Interpolate.EaseType moveToSwingPositionEase = Interpolate.EaseType.InOutQuad;
	
	[Space(10)]
	public float moveToNormalPositionTime = 1;
	public Interpolate.EaseType moveToNormalPositionEase = Interpolate.EaseType.InOutQuad;

	private int currentIndex;
	private BattleCharacter[] characters;
	private CharacterSheet[] characterSheets {
		get { return GameManager.party; }
	}

	void OnEnable () {
		BattleMenu.onActionSelected += OnActionSelected;
		SwingMeter.onChargeCancelled += OnChargeCancelled;
		SwingMeter.onChargeStarted += OnChargeStarted;
		SwingMeter.onChargeFinished += OnChargeFinished;
		BattleCharacter.onBallFlightFinished += OnBallFlightFinished;
		BattleCharacter.onTurnFinished += OnTurnFinished;
		Enemy.onEnemyDeath += OnEnemyDeath;
	}

	void OnDisable () {
		BattleMenu.onActionSelected -= OnActionSelected;
		SwingMeter.onChargeCancelled -= OnChargeCancelled;
		SwingMeter.onChargeStarted -= OnChargeStarted;
		SwingMeter.onChargeFinished -= OnChargeFinished;
		BattleCharacter.onBallFlightFinished += OnBallFlightFinished;
		BattleCharacter.onTurnFinished -= OnTurnFinished;
		Enemy.onEnemyDeath -= OnEnemyDeath;
	}

	void Awake () {
		LoadCharacters();
	}

	void Start () {
		if (cam == null) cam = Camera.main;
		enemyHealthBar.SetActive(false);
		swingMeter.gameObject.SetActive(false);
		battleMenu.gameObject.SetActive(false);
		StartCoroutine("TransitionInCoroutine");
	}

	int IndexFromSheet(CharacterSheet characterSheet) {
		for (int i = 0; i < characterSheets.Length; i++) {
			if (characterSheets[i] == characterSheet) return i;
		}
		return 0;
	}

	[ContextMenu("Load Characters")]
	void LoadCharacters () {
		playersRoot.DestroyChildren();
		int len = characterSheets.Length;
		characters = new BattleCharacter[len];
		Vector3 scale = NormalScale();
		Vector3[] positions = NormalPositions();
		for (int i = 0; i < len; i++) {
			CharacterSheet sheet = characterSheets[i];
			GameObject avatar = sheet.battlePrefab.Instantiate() as GameObject;
			characters[i] = avatar.GetComponent<BattleCharacter>();
			avatar.transform.parent = playersRoot;
			avatar.transform.localPosition = positions[i];
			avatar.transform.localScale = scale;
		}
	}

	IEnumerator TransitionInCoroutine () {
		Vector3 lowerLeft = cam.ViewportToWorldPoint(Vector3.zero);
		lowerLeft.z = 0;
		Vector3 upperRight = cam.ViewportToWorldPoint(new Vector3(1,1,0));
		upperRight.z = 0;
		Vector3 disp = upperRight - lowerLeft;
		float width = disp.x;
		float height = disp.y;
		
		float t = 0;
		while (t < transitionInTime) {
			t += Time.deltaTime;
			float frac = Interpolate.Ease(transitionInEase, Mathf.Clamp01(t / transitionInTime));
			enemiesRoot.position = Vector3.Lerp(
				upperRight + new Vector3(width, -height*0.5f, 0),
				lowerLeft + new Vector3(width*0.25f, height*0.5f, 0),
				frac
			);
			playersRoot.position = Vector3.Lerp(
				lowerLeft + new Vector3(-width, height*0.5f, 0),
				lowerLeft + new Vector3(width*0.75f, height*0.5f, 0),
				frac
			);
			yield return new WaitForEndOfFrame();
		}
		ShowActionMenu();
	}

	void ShowActionMenu () {
		enemyHealthBar.SetActive(true);
		battleMenu.gameObject.SetActive(true);
		swingMeter.gameObject.SetActive(false);
	}

	void OnActionSelected (CharacterSheet characterSheet, BattleAction action, string text) {
		if (action == BattleAction.Stroke) {
			battleMenu.gameObject.SetActive(false);
			swingMeter.ResetCharge();
			swingMeter.gameObject.SetActive(true);
			currentIndex = IndexFromSheet(characterSheet);
			characters[currentIndex].Ready();
			StartCoroutine("MoveToSwingPositionCoroutine", currentIndex);
		}
	}

	void OnChargeCancelled () {
		ShowActionMenu();
		characters[currentIndex].Idle();
		StartCoroutine("MoveToNormalPositionCoroutine");
	}

	void OnChargeStarted () {

	}

	void OnChargeFinished (float charge) {
		characters[currentIndex].Swing(
			characterSheets[currentIndex].currentClub, 
			charge,
			enemy
		);
	}

	// this seems unnecessary now
	void OnBallFlightFinished (GolfClub club, float modifier, Vector3 target) {
		explosion.Explode(target);
	}

	void OnTurnFinished () {
		StartCoroutine("MoveToNormalPositionCoroutine");
		ShowActionMenu();
	}

	void OnEnemyDeath (Enemy enemy) {
		// TODO: check for all enemy deaths if there are more than one
		onBattleFinish();
	}

	Vector3[] NormalPositions () {
		switch (characters.Length) {
			case 2:
				return new Vector3[] {
					new Vector3(-1.25f, -2.5f, 0),
					new Vector3(1.25f, -2.5f, 0)
				};
			case 3:
				return new Vector3[] {
					new Vector3(-1.5f, -2.5f, 0),
					new Vector3(0, -2, 0),
					new Vector3(1.5f, -2.5f, 0)
				};
			default:
				return new Vector3[] {
					new Vector3(0, -2.5f, 0)
				};
		}
	}	

	Vector3[] SwingPositions (int characterIndex) {
		Vector3[] positions = NormalPositions();
		Vector3 selectedPos = positions[characterIndex];
		for (int i = 0; i < characters.Length; i++) {
			if (i == characterIndex) {
				positions[i] = new Vector3(0, -2.5f, 0);
			}
			else {
				Vector3 diff = positions[i] - selectedPos;
				positions[i] = positions[i] + diff * 2 - Vector3.up * 6;
			}
		}
		return positions;
	}

	Vector3 NormalScale () {
		switch (characters.Length) {
			case 2:
				return Vector3.one * 0.8f;
			case 3:
				return Vector3.one * 0.6f;
			default:
				return Vector3.one;
		}
	}

	IEnumerator MoveToSwingPositionCoroutine (int characterIndex) {
		Vector3[] startPositions = System.Array.ConvertAll(characters, c => c.transform.localPosition);
		Vector3[] endPositions = SwingPositions(characterIndex);
		Vector3 startScale = NormalScale();
		Vector3 endScale = Vector3.one;
		
		float t = 0;
		while (t < moveToSwingPositionTime) {
			t += Time.deltaTime;
			float frac = Interpolate.Ease(
				transitionInEase, 
				Mathf.Clamp01(t / moveToSwingPositionTime)
			);
			for (int i = 0; i < characters.Length; i++) {
				characters[i].transform.localPosition = Vector3.Lerp(
					startPositions[i],
					endPositions[i],
					frac
				);
				characters[i].transform.localScale = Vector3.Lerp(
					startScale,
					endScale,
					frac
				);
			}
			yield return new WaitForEndOfFrame();
		}
	}

	IEnumerator MoveToNormalPositionCoroutine () {
		Vector3[] startPositions = System.Array.ConvertAll(characters, c => c.transform.localPosition);
		Vector3[] endPositions = NormalPositions();
		Vector3 startScale = Vector3.one;
		Vector3 endScale = NormalScale();

		float t = 0;
		while (t < moveToNormalPositionTime) {
			t += Time.deltaTime;
			float frac = Interpolate.Ease(
				transitionInEase, 
				Mathf.Clamp01(t / moveToNormalPositionTime)
			);
			for (int i = 0; i < characters.Length; i++) {
				characters[i].transform.localPosition = Vector3.Lerp(
					startPositions[i],
					endPositions[i],
					frac
				);
				characters[i].transform.localScale = Vector3.Lerp(
					startScale,
					endScale,
					frac
				);
			}
			yield return new WaitForEndOfFrame();
		}
	}
}
