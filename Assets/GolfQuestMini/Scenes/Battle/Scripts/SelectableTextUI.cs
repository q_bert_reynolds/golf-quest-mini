﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class SelectableTextUI : MonoBehaviour, ISelectHandler, ISubmitHandler, IPointerClickHandler {

	public delegate void OnTextHighlighted (RectTransform rectTransform);
	public static event OnTextHighlighted onTextHighlighted = delegate {};

	public delegate void OnTextSelected (string text);
	public static event OnTextSelected onTextSelected = delegate {};

	public Text text;
	
	public void OnSelect (BaseEventData eventData) {
		onTextHighlighted(gameObject.GetComponent<RectTransform>());
	}

	public void OnSubmit (BaseEventData eventData) {
		onTextSelected(text.text);
	}

	public void OnPointerClick (PointerEventData eventData) {
		onTextSelected(text.text);
	}
}
