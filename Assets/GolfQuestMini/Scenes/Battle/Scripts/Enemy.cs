﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public delegate void OnEnemyDeath (Enemy enemy);
	public static event OnEnemyDeath onEnemyDeath = delegate {};

	public EnemySheet stats;
	public EnemyInfoUI enemyInfoUI;
	private Animator animator;
	public bool isVulnerable = false;

	void Awake () {
		animator = GetComponent<Animator>();
	}

	public void TakeDamage (int damage) {
		if (!isVulnerable) {
			
		}

		if (damage == 0) return;
		else if (damage == stats.currentHealth) {
			onEnemyDeath(this);
			animator.Play("Die");
		}
		else animator.Play("TakeHit");
		enemyInfoUI.AnimateHealth(damage);
	}
}
