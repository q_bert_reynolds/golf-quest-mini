﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Paraphernalia.Extensions;
using Paraphernalia.Utils;

public class BattleMenu : MonoBehaviour {

	public delegate void OnActionSelected (CharacterSheet character, BattleAction action, string text);
	public static event OnActionSelected onActionSelected = delegate {};

	public AudioClip highlightClip;
	public AudioClip selectClip;
	public GameObject menuItemsRoot;
	public GameObject characterInfoPrefab;
	public GameObject textPrefab;
	
	private enum SelectionMenu {
		Character,
		Action,
		Stroke,
		Tech,
		Item
	}

	private SelectionMenu currentSelectionMenu = SelectionMenu.Character;
	
	private string[] actionsTitles = new string[]{"Stroke", "Tech", "Item", "Mulligan"}; // localization?
	
	private List<CharacterSheet> charSelectOrder = new List<CharacterSheet>();
	
	private CharacterSheet[] characterSheets {
		get { return GameManager.party; }
	}

	private CharacterSheet currentCharacter {
		get { 
			if (Application.isPlaying) {
				if (charSelectOrder.Count == 0) return null;
				return charSelectOrder[charSelectOrder.Count-1];
			}
			else {
				return GameManager.party[0];
			}
		}
	}
	
	private ScrollRect scrollRect;
	private Mask mask;
	
	void OnEnable () {
		CharacterInfoUI.onCharacterSelected += OnCharacterSelected;
		CharacterInfoUI.onCharacterHighlighted += OnCharacterHighlighted;
		SelectableTextUI.onTextSelected += OnTextSelected;
		SelectableTextUI.onTextHighlighted += OnTextHighlighted;
	}

	void OnDisable () {
		CharacterInfoUI.onCharacterSelected -= OnCharacterSelected;
		CharacterInfoUI.onCharacterHighlighted -= OnCharacterHighlighted;
		SelectableTextUI.onTextSelected -= OnTextSelected;
		SelectableTextUI.onTextHighlighted -= OnTextHighlighted;
	}

	void Start () {
		ShowCharacterSelect();
	}

	void Update () {
		if (Input.GetButton("Cancel")) OnBackPressed();
	}

	void OnCharacterSelected (CharacterSheet character) {
		GetComponent<AudioSource>().clip = selectClip;
		GetComponent<AudioSource>().Play();
		charSelectOrder.Add(character);
		ShowActionSelect();
	}

	void OnTextSelected (string text) {
		GetComponent<AudioSource>().clip = selectClip;
		GetComponent<AudioSource>().Play();
		if (currentSelectionMenu == SelectionMenu.Action) {
			if (text == actionsTitles[0]) {
				ShowStrokeSelect();
			}
			else if (text == actionsTitles[1]) {
				ShowTechSelect();
			}
			else if (text == actionsTitles[2]) {
				ShowItemSelect();
			}
			else if (text == actionsTitles[3]) {
				Debug.Log("NO MULLIGANS");
			}
			else {
				Debug.LogError("Action menu item '" + text + "' does not exist");
			}
		}
		else if (currentSelectionMenu == SelectionMenu.Stroke) {
			onActionSelected(currentCharacter, BattleAction.Stroke, text);
			currentCharacter.SetCurrentAction(text);
			ShowCharacterSelect();
		}
		else if (currentSelectionMenu == SelectionMenu.Tech) {
			ShowCharacterSelect();
		}
		else if (currentSelectionMenu == SelectionMenu.Item) {
			ShowCharacterSelect();
		}
	}

	void OnCharacterHighlighted (CharacterSheet characterSheet) {
		GetComponent<AudioSource>().clip = highlightClip;
		GetComponent<AudioSource>().Play();
	}

	void OnTextHighlighted (RectTransform r) {
		GetComponent<AudioSource>().clip = highlightClip;
		GetComponent<AudioSource>().Play();
		if (scrollRect == null) return;

		RectTransform s = scrollRect.content;
		RectTransform m = mask.rectTransform;

		Vector3[] corners = new Vector3[4];
		r.GetWorldCorners(corners);
		float rTop = corners[1].y;
		float rBottom = corners[0].y;

		m.GetWorldCorners(corners);
		float mTop = corners[1].y;
		float mBottom = corners[0].y;

		if (rTop > mTop) {
			s.position += Vector3.up * (mTop - rTop);
		}
		else if (rBottom < mBottom) {
			s.position += Vector3.up * (mBottom - rBottom);
		}
	}

	void OnBackPressed () {
		if (currentSelectionMenu == SelectionMenu.Action) {
			charSelectOrder.Remove(currentCharacter);
			ShowCharacterSelect();
		}
		else if (currentSelectionMenu == SelectionMenu.Stroke || 
			currentSelectionMenu == SelectionMenu.Tech || 
			currentSelectionMenu == SelectionMenu.Item) {
			ShowActionSelect();
		}
	}

	void ClearSelectBox () {
		menuItemsRoot.transform.DestroyChildren();
		GameObjectUtils.Destroy(scrollRect);
		GameObjectUtils.Destroy(mask);
		scrollRect = null;
		mask = null;
	}

	[ContextMenu("Show Character Select")]
	void ShowCharacterSelect () {
		if (charSelectOrder.Count == characterSheets.Length) {
			charSelectOrder.Clear();
		}
		ClearSelectBox();
		currentSelectionMenu = SelectionMenu.Character;
		int len = characterSheets.Length;
		for (int i = 0; i < len; i++) {
			GameObject go = characterInfoPrefab.Instantiate() as GameObject;
			CharacterInfoUI charInfo = go.GetComponent<CharacterInfoUI>();
			charInfo.characterSheet = characterSheets[i];
			go.transform.SetParent(menuItemsRoot.transform);
			go.transform.localScale = Vector3.one;
			go.transform.localPosition = Vector3.zero;
			charInfo.UpdateInfo(); 
			if (charSelectOrder.Contains(characterSheets[i])) {
				go.GetComponent<Selectable>().interactable = false;
				go.GetComponent<Animator>().Play("Disabled");
			}
			else {
				go.GetComponent<Selectable>().interactable = true;
				go.GetComponent<Animator>().Play("Normal");
			}
			if (i == 0) EventSystemSingleton.SetSelectedObject(go);
		}
	}

	[ContextMenu("Show Action Select")]
	void ShowActionSelect () {
		ClearSelectBox();
		currentSelectionMenu = SelectionMenu.Action;
		for (int i = 0; i < 2; i++) {
			GameObject row = new GameObject("row" + i);
			Image image = row.AddComponent<Image>();
			image.color = Color.clear;
			row.AddComponent<HorizontalLayoutGroup>();
			row.transform.SetParent(menuItemsRoot.transform);
			row.transform.localScale = Vector3.one;
			row.transform.localPosition = Vector3.zero;
			for (int j = 0; j < 2; j++) {
				GameObject go = textPrefab.Instantiate() as GameObject;
				SelectableTextUI t = go.GetComponent<SelectableTextUI>();
				t.text.text = actionsTitles[2*i+j];;
				go.transform.SetParent(row.transform);
				go.transform.localScale = Vector3.one;
				go.transform.localPosition = Vector3.zero;
				if (i+j == 0) EventSystemSingleton.SetSelectedObject(go);
			}
		}
	}

	[ContextMenu("Show Stroke Select")]
	void ShowStrokeSelect () {
		ClearSelectBox();
		currentSelectionMenu = SelectionMenu.Stroke;
		string[] clubNames = System.Array.ConvertAll(currentCharacter.clubs, club => club.clubName);
		ShowScrollableList(clubNames);
	}

	[ContextMenu("Show Tech Select")]
	void ShowTechSelect () {
		ClearSelectBox();
		currentSelectionMenu = SelectionMenu.Tech;
		
	}

	[ContextMenu("Show Item Select")]
	void ShowItemSelect () {
		ClearSelectBox();
		currentSelectionMenu = SelectionMenu.Item;
	}

	void ShowScrollableList (string[] text) {
		scrollRect = menuItemsRoot.GetOrAddComponent<ScrollRect>();
		scrollRect.horizontal = false;
		scrollRect.vertical = true;

		GameObject maskObj = new GameObject("mask");
		Image image = maskObj.AddComponent<Image>();
		image.color = Color.white;
		mask = maskObj.AddComponent<Mask>();
		mask.showMaskGraphic = false;
		maskObj.transform.SetParent(menuItemsRoot.transform);
		maskObj.transform.localScale = Vector3.one;
		maskObj.transform.localPosition = Vector3.zero;

		GameObject scrollable = new GameObject("scrollable");
		scrollable.transform.SetParent(maskObj.transform);
		scrollable.transform.localScale = Vector3.one;
		scrollable.transform.localPosition = Vector3.zero;
		
		RectTransform scrollRectTransform = scrollable.GetOrAddComponent<RectTransform>();
		scrollRectTransform.anchorMin = Vector2.zero;
		scrollRectTransform.anchorMax = Vector2.one;
		scrollRectTransform.offsetMin = Vector2.zero;
		scrollRectTransform.offsetMax = Vector2.zero;
		scrollRect.content = scrollRectTransform;

		scrollable.AddComponent<VerticalLayoutGroup>();
		
		for (int i = 0; i < text.Length; i++) {
			GameObject go = textPrefab.Instantiate() as GameObject;
			SelectableTextUI t = go.GetComponent<SelectableTextUI>();
			t.text.text = text[i];;
			go.transform.SetParent(scrollable.transform);
			go.transform.localScale = Vector3.one;
			go.transform.localScale = Vector3.one;
			if (i == 0) EventSystemSingleton.SetSelectedObject(go);
		}
	}
}
