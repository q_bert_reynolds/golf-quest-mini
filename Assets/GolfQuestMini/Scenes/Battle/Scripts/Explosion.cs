﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	private Animator animator;

	void Awake () {
		animator = GetComponent<Animator>();
	}
	
	public void Explode (Vector3 pos) {
		transform.position = pos;
		transform.localScale = Vector3.one * Random.Range(0.9f, 1.1f);
		transform.localEulerAngles = Vector3.forward * Random.Range(0, 360f);
		animator.Play("Explode", -1, 0);
	}
}
