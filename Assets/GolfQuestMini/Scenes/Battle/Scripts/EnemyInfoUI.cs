﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class EnemyInfoUI : MonoBehaviour {

	public EnemySheet stats;
	public Text nameText;
	public Text levelText;
	public Transform healthBar;

	public float damageAnimationSpeed = 10f;

	void Start () {
		UpdateInfo();
	}
	
	[ContextMenu("Update Info")]
	public void UpdateInfo () {
		nameText.text = stats.enemyName;
		levelText.text = "Lv " + stats.level;
		float frac = (float)stats.currentHealth / (float)stats.health;
		healthBar.localScale = new Vector3(frac, 1, 1);
	}

	public void AnimateHealth (int damage) {
		StartCoroutine("AnimateHealthCoroutine", damage);
	}

	IEnumerator AnimateHealthCoroutine (int damage) {
		int originalHealth = stats.currentHealth;
		float damageSoFar = 0;
		while (damageSoFar < damage) {
			damageSoFar += Time.deltaTime * damageAnimationSpeed;
			stats.currentHealth = originalHealth - (int)damageSoFar;
			UpdateInfo();
			yield return new WaitForEndOfFrame ();
		}

		if (stats.currentHealth < 0) {
			int blinkCount = 3;
			for (int i = 0; i < blinkCount; i++) {
				stats.currentHealth = 0;
				UpdateInfo();			
				yield return new WaitForSeconds(0.2f);
				stats.currentHealth = originalHealth - damage;
				UpdateInfo();	
				yield return new WaitForSeconds(0.2f);		
			}	
			yield return new WaitForSeconds(0.1f);
			stats.currentHealth = Mathf.Abs(originalHealth - damage);
			UpdateInfo();			
		}
	}
}
