using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class ResolutionSelectable : Selectable, ISelectHandler, ISubmitHandler, IPointerClickHandler {
	
	public Text text;

	int prevIndex = 0;
	int index = 0;

	List<Resolution> resolutions;
	
	protected override void Start () {
		base.Start();
		resolutions = new List<Resolution>(Screen.resolutions);
		// float ratio = (float)Screen.width / (float)Screen.height;
		// resolutions.RemoveAll((res) => ((float)res.width / (float)res.height != ratio));
		resolutions.Sort((x, y) => (x.width * x.height).CompareTo(y.width * y.height));
		
		index = resolutions.IndexOf(Screen.currentResolution);
		prevIndex = index;
		UpdateText();
	}
	
	public override Selectable FindSelectableOnLeft () {
		index--;
		if (index < 0) index = 0;
		UpdateText();
		return this;
	}
	
	public override Selectable FindSelectableOnRight () {
		index++;
		if (index >= resolutions.Count) index = resolutions.Count - 1;
		UpdateText();
		return this;
	}
	
	public override void OnDeselect (BaseEventData eventData) {
		base.OnDeselect(eventData);
		index = resolutions.IndexOf(Screen.currentResolution);
		UpdateText();
	}

	public override void OnSelect (BaseEventData eventData) {
		base.OnSelect(eventData);
		SetResolution();
	}

	public void OnSubmit (BaseEventData eventData) {
		SetResolution();
	}

	public void OnPointerClick (PointerEventData eventData) {
		SetResolution();
	}

	void UpdateText () {
		text.text = string.Format("{0} x {1}", resolutions[index].width, resolutions[index].height);
	}
	
	public void SetResolution () {
		if (index == prevIndex) return;
		prevIndex = index;
		Screen.SetResolution(resolutions[index].width, resolutions[index].height, Screen.fullScreen);
	}
}
