﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class SubmitButton : MonoBehaviour, ISubmitHandler {

	public delegate void Submit (SubmitButton button);
	public static event Submit onSubmit = delegate {};

	public void OnSubmit (BaseEventData eventData) {
		onSubmit(this);
	}
}
