﻿using UnityEngine;
using System.Collections;
using Paraphernalia.Extensions;

public class CloudGenerator : MonoBehaviour {

	public Bounds bounds;

	public float spawnRate = 2;
	private int currentIndex = 0;
	private Cloud[] clouds;

	void Awake () {
		clouds = GetComponentsInChildren<Cloud>();
	}

	void OnEnable () {
		StartCoroutine("SpawnClouds");
	}

	IEnumerator SpawnClouds () {
		while (enabled) {
			Cloud cloud = clouds[currentIndex];
			if (!cloud.gameObject.activeSelf) {
				cloud.Spawn(transform.position + bounds.RandomPoint());
				yield return new WaitForSeconds(spawnRate);
			}
			currentIndex = (currentIndex + 1) % clouds.Length;
			yield return new WaitForEndOfFrame();
		}
	}

	void OnDrawGizmos () {
		Gizmos.matrix = transform.localToWorldMatrix;
		Gizmos.DrawWireCube(bounds.center, bounds.size);
	}
}
