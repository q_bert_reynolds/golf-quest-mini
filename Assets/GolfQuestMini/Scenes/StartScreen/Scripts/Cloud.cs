﻿using UnityEngine;
using System.Collections;
using Paraphernalia.Extensions;

public class Cloud : MonoBehaviour {

	public Vector3 velocity = Vector3.right;
	public float scaleVariation = 0.1f;
	public float speedVariation = 0.01f;
	public float minAlpha = 0.9f;

	private Vector3 currentVelocity;
	private float farRight;

	private SpriteRenderer _spriteRenderer;
	private SpriteRenderer spriteRenderer {
		get {
			if (_spriteRenderer == null) {
				_spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
			}
			return _spriteRenderer;
		}
	}

	void Awake () {
		farRight = Camera.allCameras[0].ViewportToWorldPoint(Vector3.one).x;
		currentVelocity = velocity * Random.Range(1-speedVariation, 1+speedVariation);
	}

	public void Spawn (Vector3 position) {
		transform.position = position;
		transform.localScale = Vector3.one * Random.Range(1-scaleVariation, 1+scaleVariation);
		currentVelocity = velocity * Random.Range(1-speedVariation, 1+speedVariation);
		spriteRenderer.color = Color.white.SetAlpha(Random.Range(minAlpha, 1));
		gameObject.SetActive(true);
	}

	void Update () {
		if (GetComponent<Renderer>().bounds.min.x < farRight) {
			transform.Translate(currentVelocity * Time.deltaTime);
		}
		else {
			gameObject.SetActive(false);
		}
	}
}
