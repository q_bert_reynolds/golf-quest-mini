﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MainMenu : MonoBehaviour {

	public GameObject title;
	public GameObject rootMenu;
	public GameObject newGameMenu;
	public GameObject continueMenu;
	public GameObject optionsMenu;
	
	private Stack<GameObject> menuStack = new Stack<GameObject>();
	private GameObject currentMenu;

	void Start () {
		HideAll();
		currentMenu = title;
		currentMenu.SetActive(true);
	}

	void OnEnable () {
		SubmitButton.onSubmit += OnSubmit;
	}

	void OnDisable () {
		SubmitButton.onSubmit -= OnSubmit;
	}

	void OnSubmit (SubmitButton button) {
		ShowRootMenu();
	}

	void Update () {
		if (Input.GetButtonDown("Cancel")) Back();
	}

	[ContextMenu("Hide All")]
	public void HideAll () {
		title.SetActive(false);
		rootMenu.SetActive(false);
		newGameMenu.SetActive(false);
		continueMenu.SetActive(false);
		optionsMenu.SetActive(false);
	}

	[ContextMenu("Back")]
	public void Back () {
		if (menuStack.Count > 0) {
			currentMenu.SetActive(false);
			currentMenu = menuStack.Pop(); 
			currentMenu.SetActive(true);
			SelectTopItem();
		}
	}

	[ContextMenu("Show Root Menu")]
	public void ShowRootMenu () {
		title.SetActive(false);
		menuStack.Push(title);
		rootMenu.SetActive(true);
		currentMenu = rootMenu;
		SelectTopItem();
	}

	[ContextMenu("Continue Selected")]
	public void ContinueSelected () {
		rootMenu.SetActive(false);
		menuStack.Push(rootMenu);
		continueMenu.SetActive(true);
		currentMenu = continueMenu;
		SelectTopItem();
	}

	[ContextMenu("New Game Selected")]
	public void NewGameSelected () {
		rootMenu.SetActive(false);
		menuStack.Push(rootMenu);
		newGameMenu.SetActive(true);
		currentMenu = newGameMenu;
		SelectTopItem();
	}

	[ContextMenu("Options Selected")]
	public void OptionsSelected () {
		rootMenu.SetActive(false);
		menuStack.Push(rootMenu);
		optionsMenu.SetActive(true);
		currentMenu = optionsMenu;
		SelectTopItem();
	}

	[ContextMenu("Select Top Item")]
	public void SelectTopItem () {
		Selectable[] selectables = currentMenu.GetComponentsInChildren<Selectable>() as Selectable[];
		if (selectables.Length == 0) return;

		float y = Mathf.NegativeInfinity;
		Selectable selectable = null;
		foreach (Selectable s in selectables) {
			Vector3[] corners = new Vector3[4];
			s.gameObject.GetComponent<RectTransform>().GetWorldCorners(corners);
			if (corners[1].y > y && s.interactable) {
				y = corners[1].y;
				selectable = s;
			}
		}

		EventSystemSingleton.SetSelectedObject(selectable.gameObject);
	}
}
