﻿using UnityEngine;
using System.Collections;

public class GolfClub : ScriptableObject {

	public string clubName = "Mythril Driver";
	public bool isPutter = false;
	public bool rightHanded = true;
	[Range(1, 60)] public float loft = 15;
	[Range(10, 60)] public float length = 40;
	[Range(0, 1)] public float flex = 0.1f;
	[Range(1, 100)] public float momentOfInertia = 10;
	[Range(100, 500)] public float headWeight = 250;
	[Range(0, 1)] public float verticalCenterOfGravity = 0.5f;

	public float baseDamage {
		get {
			return 1 + 0.1f * (1f + flex) * headWeight * length / loft;
		}
	}

	public float accuracy {
		get {
			float f = 1 - flex * 0.2f;
			float v = 1 - Mathf.Abs(0.5f - verticalCenterOfGravity) * 0.1f;
			float m = 1 - momentOfInertia / 100f;
			return f * v * m;
		}
	}
}
