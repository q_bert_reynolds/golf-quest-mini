﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	private static GameManager _instance;
	public static GameManager instance {
		get {
			if (_instance == null) {
				_instance = GameObject.FindObjectOfType(typeof(GameManager)) as GameManager;
			}
			if (_instance == null) {
				GameObject go = new GameObject("GameManager");
				_instance = go.AddComponent<GameManager>();
			}

			return _instance;
		}
	}

	public CharacterSheet[] currentParty;

	public GameObject currentScene {
		get { 
			if (scenes.Count > 0) return scenes[0];
			return null;
		}
	}

	public GameObject previousScene {
		get { 
			if (scenes.Count > 1) return scenes[1];
			return null;
		}
	}

	public List<GameObject> scenes = new List<GameObject>();

	void Awake () {
		if (_instance == null) {
			_instance = this;
		}
		else {
			Debug.LogWarning("GameManager already exists, deleting.");
			GameObject.DestroyImmediate(gameObject);
		}
	}

	void Start () {
		GameObject scene = GameObject.Find(SceneManager.GetActiveScene().name);
		scenes.Add(scene);
	}

	public static CharacterSheet[] party {
		get {
			return instance.currentParty;
		}
	}

	[ContextMenu("Back")]
	public void Back () {
		ShowPreviousScene();
	}

	public static void ShowPreviousScene () {
		if (instance.previousScene == null) return;
		instance.currentScene.SetActive(false);
		instance.previousScene.SetActive(true);
		GameObject temp = instance.currentScene;
		instance.scenes[0] = instance.previousScene;
		instance.scenes[1] = temp;
	}

	public static void LoadScene(string name) {
		instance.StartCoroutine("LoadSceneAsync", name);
	}

	public IEnumerator LoadSceneAsync(string name) {
		GameObject scene = instance.scenes.Find((g) => g.name == name);
		if (scene == null) {
			yield return SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
			scene = GameObject.Find(name);	
		}
		else instance.scenes.Remove(scene);

		instance.scenes.Insert(0, scene);
		foreach (GameObject s in instance.scenes) {
			s.SetActive(s == scene);
		}
	}
}
