﻿using UnityEngine;
using System.Collections;

public class OutsideAnimationController : MonoBehaviour {

	public float footSpacing = 0.5f;

	public string characterName = "Ace";
	public string setName = "Small";
	public int frames = 4;
	public string[] directionNames = new string[] {
		"East", "NorthEast", "North", "NorthWest", "West", "SouthWest", "South", "SouthEast"
	};
	
	private int frameIndex;
	private int dirIndex;
	private int dirCount;
	private float distTraveled = 0;
	
	private SpriteRenderer _spriteRenderer;
	public SpriteRenderer spriteRenderer {
		get {
			if (_spriteRenderer == null) {
				_spriteRenderer = GetComponent<SpriteRenderer>();
			}
			return _spriteRenderer;
		}
	}

	void Awake () {
		dirCount = directionNames.Length;
	}

	void Update () {
		Vector2 v = GetComponent<Rigidbody2D>().velocity;
		distTraveled += v.magnitude * Time.deltaTime;
		string frameName = "stand";
		if (v.magnitude > 0.001f) {
			Vector2 heading = v.normalized;
			float ang = 360 + Mathf.Atan2(heading.y, heading.x) * Mathf.Rad2Deg;
			dirIndex = Mathf.RoundToInt(ang * (float)dirCount / 360f) % dirCount;
			// frameIndex = 2 * (int)((distTraveled / footSpacing) % frames) % frames + 1;
			frameIndex = (int)((distTraveled / footSpacing) % frames) % frames;
			frameName = "walk" + frameIndex;
		}

		string path = "Characters/" + characterName + "/" + setName + "/" + directionNames[dirIndex] + "/";
		path += "" + characterName + "_" + setName + "_" + directionNames[dirIndex] + "_" + frameName;
		spriteRenderer.sprite = Resources.Load<Sprite>(path);
	}
}
