﻿using UnityEngine;
using System.Collections;

public class RotateWithSpeed : MonoBehaviour {

	public float speed = 0;
	public Vector3 axis = Vector3.forward;

	void Update () {
		transform.Rotate(axis * speed * Time.deltaTime);
	}
}
