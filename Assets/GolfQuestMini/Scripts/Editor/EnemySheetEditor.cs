using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CanEditMultipleObjects]
[CustomEditor(typeof(EnemySheet))]
public class EnemySheetEditor : Editor {

	EnemySheet enemySheet {
		get { return target as EnemySheet; }
	}

	EnemySheet[] enemySheets {
		get { return targets as EnemySheet[]; }
	}

	[MenuItem("GameObject/Create Other/Golf Quest Mini/Enemy Sheet")]
	static void CreateEnemySheet () {
		ScriptableObjectUtility.CreateAsset<EnemySheet>();
	}

	public override void OnInspectorGUI () {
		base.OnInspectorGUI();

		if (target == null) return;
		EditorGUILayout.BeginHorizontal();

		EditorGUILayout.BeginVertical();
		EditorGUILayout.LabelField("health");
		EditorGUILayout.LabelField("strength");
		EditorGUILayout.LabelField("defense");
		EditorGUILayout.LabelField("speed");
		EditorGUILayout.LabelField("special");
		EditorGUILayout.LabelField("accuracy");
		EditorGUILayout.EndVertical();
		
		EditorGUILayout.BeginVertical();
		EditorGUILayout.LabelField("" + enemySheet.health);
		EditorGUILayout.LabelField("" + enemySheet.strength);
		EditorGUILayout.LabelField("" + enemySheet.defense);
		EditorGUILayout.LabelField("" + enemySheet.speed);
		EditorGUILayout.LabelField("" + enemySheet.special);
		EditorGUILayout.LabelField("" + enemySheet.accuracy);
		EditorGUILayout.EndVertical();

		EditorGUILayout.EndHorizontal();
	}
}