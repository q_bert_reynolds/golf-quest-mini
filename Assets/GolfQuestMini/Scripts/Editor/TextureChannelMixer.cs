﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using Paraphernalia.Extensions;

public class TextureChannelMixer : EditorWindow {

	Texture2D tex1;
	Texture2D tex2;
	Vector4 values = Vector4.one;
	Texture2D tex;

	[MenuItem ("Window/Texture Channel Mixer")]
	public static void Create () {
		EditorWindow.GetWindow(typeof(TextureChannelMixer));
	}

	void OnGUI () {
		tex1 = EditorGUILayout.ObjectField("Texture1", tex1, typeof(Texture2D), false) as Texture2D;
		tex2 = EditorGUILayout.ObjectField("Texture2", tex2, typeof(Texture2D), false) as Texture2D;
		values[0] = EditorGUILayout.Slider("R", values[0], 0, 1);
		values[1] = EditorGUILayout.Slider("G", values[1], 0, 1);
		values[2] = EditorGUILayout.Slider("B", values[2], 0, 1);
		values[3] = EditorGUILayout.Slider("A", values[3], 0, 1);
		if (tex1 && tex2 && GUILayout.Button("Mix")) Mix();
		if (tex && GUILayout.Button("Save")) {
			tex.SaveToPNG("Assets/blah.png");
		}
		if (tex != null) {
			Rect r = GUILayoutUtility.GetLastRect();
			r.y = r.y + r.height;
			r.height = r.width;
			EditorGUI.DrawPreviewTexture(r, tex);
		}
	}

	public void Mix() {
		int height = tex1.height;
		int width = tex1.width;
		if (tex == null || tex.height != height || tex.width != width)
			tex = new Texture2D(height, width, tex2.format, false);
		
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				Color c1 = tex1.GetPixel(i,j);
				Color c2 = tex2.GetPixel(i,j);
				Color c = new Color(
					Mathf.Lerp(c1.r, c2.r, values[0]),
					Mathf.Lerp(c1.g, c2.g, values[1]),
					Mathf.Lerp(c1.b, c2.b, values[2]),
					Mathf.Lerp(c1.a, c2.a, values[3])
				);
				tex.SetPixel(i, j, c);
			}
		}

		tex.Apply();
	}
}
