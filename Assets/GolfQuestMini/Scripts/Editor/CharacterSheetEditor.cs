using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CanEditMultipleObjects]
[CustomEditor(typeof(CharacterSheet))]
public class CharacterSheetEditor : Editor {

	CharacterSheet characterSheet {
		get { return target as CharacterSheet; }
	}

	CharacterSheet[] characterSheets {
		get { return targets as CharacterSheet[]; }
	}

	[MenuItem("GameObject/Create Other/Golf Quest Mini/Character Sheet")]
	static void CreateCharacterSheet () {
		ScriptableObjectUtility.CreateAsset<CharacterSheet>();
	}

	public override void OnInspectorGUI () {
		base.OnInspectorGUI();

		if (target == null) return;
		EditorGUILayout.BeginHorizontal();

		EditorGUILayout.BeginVertical();
		EditorGUILayout.LabelField("level");
		EditorGUILayout.LabelField("health");
		EditorGUILayout.LabelField("strength");
		EditorGUILayout.LabelField("defense");
		EditorGUILayout.LabelField("speed");
		EditorGUILayout.LabelField("special");
		EditorGUILayout.LabelField("accuracy");
		EditorGUILayout.EndVertical();
		
		EditorGUILayout.BeginVertical();
		EditorGUILayout.LabelField("" + characterSheet.level);
		EditorGUILayout.LabelField("" + characterSheet.health);
		EditorGUILayout.LabelField("" + characterSheet.strength);
		EditorGUILayout.LabelField("" + characterSheet.defense);
		EditorGUILayout.LabelField("" + characterSheet.speed);
		EditorGUILayout.LabelField("" + characterSheet.special);
		EditorGUILayout.LabelField("" + characterSheet.accuracy);
		EditorGUILayout.EndVertical();

		EditorGUILayout.EndHorizontal();
	}
}