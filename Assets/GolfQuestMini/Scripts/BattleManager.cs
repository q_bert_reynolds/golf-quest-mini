﻿using UnityEngine;
using System.Collections;
using Paraphernalia.Extensions;
using Paraphernalia.Utils;

public class BattleManager : MonoBehaviour {

	public Camera cam;
	public Material transitionMaterial;
	public float transitionTime = 1;
	public Interpolate.EaseType transitionEase = Interpolate.EaseType.InOutQuad;

	private PlayerController player;

	void OnEnable () {
		BattleTrigger.onBattleStart += OnBattleStart;
	}

	void OnDisable () {
		BattleTrigger.onBattleStart -= OnBattleStart;
	}

	void Start () {
		if (cam == null) cam = Camera.main;
		player = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
		transitionMaterial.SetFloat("_Radius", 1000);
	}

	void OnBattleStart (EnemySheet sheet) {
		player.Pause();
		StartCoroutine("TransitionToBattleCoroutine", sheet);
	}

	IEnumerator TransitionToBattleCoroutine (EnemySheet sheet) {
		Bounds b = player.gameObject.RendererBounds();
		Vector3 min = cam.WorldToScreenPoint(b.min);
		Vector3 max = cam.WorldToScreenPoint(b.max);
		Vector3 c = (min + max) * 0.5f;
		transitionMaterial.SetVector("_Params", (Vector4)c);

		float startRad = (float)Screen.width;
		float endRad = (max - min).magnitude;
		float t = 0;
		while (t < transitionTime) {
			t += Time.deltaTime;
			float frac = Interpolate.Ease(transitionEase, Mathf.Clamp01(t / transitionTime));
			float r = Mathf.Lerp(startRad, endRad, frac);
			transitionMaterial.SetFloat("_Radius", r);
			yield return new WaitForEndOfFrame();
		}

		GameManager.LoadScene("Battle");
		transitionMaterial.SetFloat("_Radius", startRad);
	}
}
