using UnityEngine;
using System.Collections;
using Paraphernalia.Extensions;

public class CharacterSheet : ScriptableObject {

	public string characterName = "Ace";
	[Multiline] public string description = "";
	public bool rightHanded = true;
	[Range(1, 10)] public float height = 5.5f;
	[Range(0, 1000000)] public int experience = 0;
	public GolfClub[] clubs;
	public int currentHealth = 20;
	
	[ResourcePath(typeof(GameObject))] public string battlePrefabPath;
	private GameObject _battlePrefab;
	public GameObject battlePrefab {
		get {
			if (_battlePrefab == null) {
				_battlePrefab = Resources.Load(battlePrefabPath, typeof(GameObject)) as GameObject;
			}
			return _battlePrefab;
		}
	}

	public void UnloadBattlePrefab () {
		Resources.UnloadAsset(_battlePrefab);
		_battlePrefab = null;
	}

	[ResourcePath(typeof(GameObject))] public string overworldPrefabPath;
	private GameObject _overworldPrefab;
	public GameObject overworldPrefab {
		get {
			if (_overworldPrefab == null) {
				_overworldPrefab = Resources.Load(overworldPrefabPath, typeof(GameObject)) as GameObject;
			}
			return _overworldPrefab;
		}
	}

	public void UnloadOverworldPrefab () {
		Resources.UnloadAsset(_overworldPrefab);
		_overworldPrefab = null;
	}

	private string currentAction;
	public GolfClub currentClub {
		get {
			foreach (GolfClub club in clubs) {
				if (club.clubName == currentAction) {
					return club;
				}
			}
			return null;
		}
	}

	public void SetCurrentAction (string action) {
		currentAction = action;
	}

	public bool CanUseClub (GolfClub club) {
		return (rightHanded != club.rightHanded);

		// length calculations

		// elemental stuff
	}

	// totally jacked from pokemon
	public int CalculateDamage (EnemySheet enemyStats, float modifier) {
		float baseDamage = currentClub.baseDamage;
		float hitRatio = (float)strength / (float)enemyStats.defense;
		float lvlRatio = (2f * (float)level + 10f) / 250f;
		return Mathf.CeilToInt((baseDamage * hitRatio * lvlRatio + 2) * modifier);
	}
	
	public int level { 
		get { return Mathf.Clamp(Mathf.FloorToInt(Mathf.Pow(experience, 1f/3f)), 1, 100); }
	}

	public AnimationCurve healthCurve = AnimationCurve.Linear(1, 20, 100, 9999);
	public int health { get { return Mathf.CeilToInt(healthCurve.Evaluate(level)); }}

	public AnimationCurve strengthCurve = AnimationCurve.Linear(1, 10, 100, 999);
	public int strength { get { return Mathf.CeilToInt(strengthCurve.Evaluate(level)); }}

	public AnimationCurve defenseCurve = AnimationCurve.Linear(1, 10, 100, 999);
	public int defense { get { return Mathf.CeilToInt(defenseCurve.Evaluate(level)); }}

	public AnimationCurve speedCurve = AnimationCurve.Linear(1, 10, 100, 999);
	public int speed { get { return Mathf.CeilToInt(speedCurve.Evaluate(level)); }}

	public AnimationCurve specialCurve = AnimationCurve.Linear(1, 10, 100, 999);
	public int special { get { return Mathf.CeilToInt(specialCurve.Evaluate(level)); }}

	public AnimationCurve accuracyCurve = AnimationCurve.Linear(1, 30, 100, 100);
	public int accuracy { get { return Mathf.CeilToInt(accuracyCurve.Evaluate(level)); }}


}
