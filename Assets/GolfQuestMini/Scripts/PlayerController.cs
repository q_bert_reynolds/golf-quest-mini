﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(InputDrivenLocomotion))]
[RequireComponent(typeof(OutsideAnimationController))]
public class PlayerController : MonoBehaviour {

	private InputDrivenLocomotion locomotion;
	private OutsideAnimationController animationController;

	void Awake () {
		locomotion = GetComponent<InputDrivenLocomotion>();
		animationController = GetComponent<OutsideAnimationController>();
	}

	public void Pause () {
		animationController.enabled = false;
		locomotion.Pause();
	}

	public void Play () {
		animationController.enabled = true;
		locomotion.Play();
	}

}
