﻿using UnityEngine;
using System.Collections;

public class FullScreenToggle : MonoBehaviour {

	public void SetFullScreen (bool isFullScreen) {
		Debug.Log("setting isFullScreen to " + isFullScreen);
		Screen.fullScreen = isFullScreen;
	}
}
