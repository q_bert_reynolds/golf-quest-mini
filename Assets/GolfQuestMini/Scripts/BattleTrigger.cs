﻿using UnityEngine;
using System.Collections;

public class BattleTrigger : MonoBehaviour {

	[ResourcePath(typeof(EnemySheet))] public string[] enemySheetPaths;
	[Range(0, 1)] public float frequency = 0.5f;
	[Range(0.01f, 10)]public float minTimeBetweenBattles = 3;

	public delegate void OnBattleStart (EnemySheet sheet);
	public static event OnBattleStart onBattleStart = delegate {};

	public static float lastBattleTime = -1;

	void OnTriggerStay2D (Collider2D collider2D) {
		if (Time.time - lastBattleTime > minTimeBetweenBattles
			&& Random.value < frequency 
			&& collider2D.gameObject.tag == "Player") {

			lastBattleTime = Time.time;
			int i = Random.Range(0, enemySheetPaths.Length - 1);
			EnemySheet enemySheet = Resources.Load(enemySheetPaths[i]) as EnemySheet;
			onBattleStart(enemySheet);
		}
	}
}
