using UnityEngine;
using System.Collections;
using Paraphernalia.Extensions;

public class EnemySheet : ScriptableObject {

	public string enemyName = "Windmill";
	[Multiline] public string description = "";
	[Range(1, 100)] public int level = 1;
	public int currentHealth = 20;
	
	[ResourcePath(typeof(GameObject))] public string battlePrefabPath;
	private GameObject _battlePrefab;
	public GameObject battlePrefab {
		get {
			if (_battlePrefab == null) {
				_battlePrefab = Resources.Load(battlePrefabPath, typeof(GameObject)) as GameObject;
			}
			return _battlePrefab;
		}
	}

	public void UnloadBattlePrefab () {
		Resources.UnloadAsset(_battlePrefab);
		_battlePrefab = null;
	}

	public AnimationCurve healthCurve = AnimationCurve.Linear(1, 20, 100, 9999);
	public int health { get { return Mathf.CeilToInt(healthCurve.Evaluate(level)); }}

	public AnimationCurve strengthCurve = AnimationCurve.Linear(1, 10, 100, 999);
	public int strength { get { return Mathf.CeilToInt(strengthCurve.Evaluate(level)); }}

	public AnimationCurve defenseCurve = AnimationCurve.Linear(1, 10, 100, 999);
	public int defense { get { return Mathf.CeilToInt(defenseCurve.Evaluate(level)); }}

	public AnimationCurve speedCurve = AnimationCurve.Linear(1, 10, 100, 999);
	public int speed { get { return Mathf.CeilToInt(speedCurve.Evaluate(level)); }}

	public AnimationCurve specialCurve = AnimationCurve.Linear(1, 10, 100, 999);
	public int special { get { return Mathf.CeilToInt(specialCurve.Evaluate(level)); }}

	public AnimationCurve accuracyCurve = AnimationCurve.Linear(1, 30, 100, 100);
	public int accuracy { get { return Mathf.CeilToInt(accuracyCurve.Evaluate(level)); }}


}
