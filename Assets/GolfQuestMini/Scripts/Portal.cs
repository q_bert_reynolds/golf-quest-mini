﻿using UnityEngine;
using System.Collections;

public class Portal : MonoBehaviour {

	public delegate void PortalTriggered (Transform traveler, string destination);
	public static event PortalTriggered portalTriggered = delegate{};

	public Vector3 offset;
	public string destinationName;
	public string sceneName;

	void OnEnable () {
		Portal.portalTriggered += OnPortalTriggered;
	}

	void OnDisable () {
		Portal.portalTriggered -= OnPortalTriggered;
	}

	void OnCollisionEnter2D (Collision2D collision) {
		if (collision.gameObject.tag == "Player") {
			GameManager.LoadScene(sceneName);
			portalTriggered(collision.transform, destinationName);
		}
	}

	void OnPortalTriggered (Transform traveler, string destination) {
		if (destination == name) {
			traveler.position = transform.position + offset;
		}
	}

	void OnDrawGizmos () {
		Gizmos.DrawWireCube(transform.position + offset, Vector3.one);
	}
}
