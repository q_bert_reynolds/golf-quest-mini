﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class InputDrivenLocomotion : MonoBehaviour {

	public float moveSpeed = 1;
	private Vector2 desiredVelocity;
	new private Rigidbody2D rigidbody2D;

	void Awake () {
		rigidbody2D = GetComponent<Rigidbody2D>();
	}

	void Update () {
		float vertical = Input.GetAxis("Vertical");
		float horizontal = Input.GetAxis("Horizontal");
		desiredVelocity = new Vector2(horizontal, vertical) * moveSpeed;
	}

	void FixedUpdate () {
		rigidbody2D.velocity = desiredVelocity;
	}

	public void Pause () {
		rigidbody2D.velocity = Vector2.zero;
		enabled = false;
	}

	public void Play () {
		enabled = true;
	}
}
