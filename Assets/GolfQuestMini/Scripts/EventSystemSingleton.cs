﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using Paraphernalia.Utils;

[RequireComponent(typeof(EventSystem))]
public class EventSystemSingleton : MonoBehaviour {

	public static EventSystemSingleton instance;

	private EventSystem eventSystem;

	void Awake () {
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad(gameObject);
			eventSystem = gameObject.GetComponent<EventSystem>();
		}
		else {
			GameObjectUtils.Destroy(this.gameObject);
		}
	}

	public static void SetSelectedObject (GameObject go) {
		if (go == null || instance == null) return;
		instance.eventSystem.SetSelectedGameObject(go, null);
	}
}
